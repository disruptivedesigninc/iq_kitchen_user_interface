#ifndef PAGEWIDGET_HPP
#define PAGEWIDGET_HPP

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QEvent>
#include <QObject>
#include <QKeyEvent>
#include <QList>
#include "widgets/control_box.hpp"


class PageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PageWidget(QWidget *parent = nullptr);

    void setTitle(const QString& title);


private:


protected:
    QHBoxLayout hLayout_;


};

#endif // PAGEWIDGET_HPP
