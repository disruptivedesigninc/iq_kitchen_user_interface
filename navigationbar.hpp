#ifndef NAVIGATIONBAR_HPP
#define NAVIGATIONBAR_HPP

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QList>
#include <QPaintEvent>
#include <QStyleOption>

class NavigationBar : public QWidget
{
    Q_OBJECT
public:
    explicit NavigationBar(int width, int height, QWidget *parent = nullptr);

    bool moveUp();
    bool moveDown();

    int currentIndex() const;

public slots:
    void hideView(bool hide);

private:
    QLabel autoPage_;
    QLabel manualPage_;
    QLabel profilePage_;
    QLabel settingsPage_;

    QList<QLabel*> navigationList_;

    int currentIndex_;


    void updateSelection(int index);

protected:
    void paintEvent(QPaintEvent *event) override;

};

#endif // NAVIGATIONBAR_HPP
