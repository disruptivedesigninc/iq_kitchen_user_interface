#include "pages/profile_page.hpp"
#include "defines.hpp"



ProfilePage::ProfilePage(QWidget *parent): PageWidget(parent),
    listView_(),
    builder_(),
    test_()
{
    this->setTitle("Cooking Profile");


    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::HEAT_GRILL, 150, 0, 0);
    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::KEEP_PRODUCT, 150, 0, 60*5);
    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::ANNOUNCE, 0, 0, 0);
    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::COOK_PRODUCT, 150, 120, 0);
    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::REST_PRODUCT, 0, 0, 60*5 + 30);


    listView_.setFixedSize(PAGE_WIDTH/3, PAGE_HEIGHT - 50);

    listView_.addListItem("Steak");
    listView_.addListItem("Fish");
    listView_.addListItem("Brisket");


    this->hLayout_.addWidget(&listView_);
    this->hLayout_.addWidget(&builder_);



    connect(&listView_, SIGNAL(itemSelectedSignal(int)), this, SLOT(itemSelectedSlot(int)));
    connect(&listView_, SIGNAL(currentRowChanged(int)), this, SLOT(itemRowChangedSlot(int)));
    connect(&builder_, SIGNAL(itemSelectedSignal(int)), this, SLOT(builderSelectedSlot(int)));
}

void ProfilePage::itemSelectedSlot(int row)
{

}

void ProfilePage::itemRowChangedSlot(int row)
{
    // When changed display information on the other screen.
    builder_.loadSequence(test_);

}

void ProfilePage::builderSelectedSlot(int row)
{
    if(row == builder_.sequence().count()){
        //open segment
        ProfileSequence::Segment segment(SEGMENT::MODE::STOP, 0, 0, 0);
        SegmentDialog dialog(segment,this);
        dialog.setFocus();
        dialog.setModal(true);
         if(dialog.exec()){
             // accepted
             builder_.sequence().addNewSegment();
             builder_.sequence().updateSegment(segment);
             builder_.buildView();
             builder_.setCurrentRow(row);
         }
    }else if(builder_.sequence().moveToSegment(row)){
        //open segment
        ProfileSequence::Segment segment = builder_.sequence().currentSegment();
        SegmentDialog dialog(segment,this);
        dialog.setFocus();
        dialog.setModal(true);
         if(dialog.exec()){
             // accepted
             builder_.sequence().updateSegment(segment);
             builder_.buildView();
             builder_.setCurrentRow(row);
         }
    }
}


