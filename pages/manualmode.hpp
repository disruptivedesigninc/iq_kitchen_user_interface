#pragma once

#include "pagewidget.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "settings.hpp"
#include "status.hpp"
#include "widgets/button_box.hpp"
#include <QTimer>
#include "widgets/spinbox.hpp"
#include "widgets/name_row.hpp"

class ManualMode: public QWidget
{
    Q_OBJECT
public:
    ManualMode(Settings& settings, Status& status, QWidget* parent = nullptr);

public slots:
    void increaseFiringRate();
    void decreaseFiringRate();
    void startBurner();
    void stopBurner();
    void increaseGrill();
    void decreaseGrill();
    void button1Press();
    void button2Press();

private slots:
    void service();

private:
    Settings& settings_;
    Status& status_;
    QLabel* title_;
    NameRow* burnerStatus_;
    NameRow* firingRate_;
    NameRow* grillHeight_;
    QList<NameRow*> temperatures_;
    NameRow* ambientTemp_;
    QList<NameRow*> inputs_;
    //QList<NameRow*> faults_;
    QList<NameRow*> outputs_;
};

