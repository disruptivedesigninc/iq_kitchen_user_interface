#pragma once

#include "pagewidget.hpp"
#include "profile/profile_sequence.hpp"
#include "widgets/list_view.hpp"
#include "QHBoxLayout"
#include "widgets/button_box.hpp"
#include "QStringList"
#include "widgets/builder_list.hpp"
#include "widgets/segment_dialog.hpp"

class ProfilePage: public PageWidget
{
    Q_OBJECT
public:
    ProfilePage(QWidget* parent = nullptr);


public slots:
    void itemSelectedSlot(int row);
    void itemRowChangedSlot(int row);

    void builderSelectedSlot(int row);



private:

    ListView listView_;
    BuilderList builder_;

    ProfileSequence test_;

};

