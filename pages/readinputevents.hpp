#pragma once

#include <QObject>
#include<iostream>
#include<fcntl.h>
#include<stdio.h>
#include<stdlib.h>
#include<linux/input.h>
#include <QFile>
#include <QSocketNotifier>
#include <QtWidgets>
#include <QTimer>
using namespace std;


class ReadInputEvents : public QObject
{
    Q_OBJECT
public:
    ReadInputEvents();

signals:
    void rotary1Up();
    void rotary1Down();
    void rotary2Up();
    void rotary2Down();
    void button1();
    void button2();
    void rotarySoundTrigger();
    void buttonSoundTrigger();

public slots:
    void service();

private:
        int fd0;
        int fd1;
        int fd2;
};

