#include "pages/manualmode.hpp"
#include "defines.hpp"
#include <QDebug>

ManualMode::ManualMode(Settings& settings, Status& status, QWidget *parent) : QWidget(parent),
    settings_(settings),
    status_(status)
{
    QHBoxLayout* hLayout_ = new QHBoxLayout();
    hLayout_->setSpacing(0);
    hLayout_->setAlignment(Qt::AlignTop);


    QVBoxLayout* colOneLayout = new QVBoxLayout();
    colOneLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    colOneLayout->setSpacing(0);
    QLabel* nameTitle = new QLabel("IQ Kitchen Demo");
    nameTitle->setStyleSheet("font-weight: bold;");

    colOneLayout->addWidget(nameTitle);
    burnerStatus_ = new NameRow("Burner", "");
    colOneLayout->addWidget(burnerStatus_);
    firingRate_ = new NameRow("Firing Rate", "%");
    colOneLayout->addWidget(firingRate_);
    grillHeight_ = new NameRow("Grill Position", "%");
    colOneLayout->addWidget(grillHeight_);


    for(size_t i = 0; i < status.temperatureArr.size(); i++){
        NameRow* row = new NameRow("Temperature "+QString::number(i+1), "°C");
        temperatures_.append(row);
        colOneLayout->addWidget(row);
    }

    ambientTemp_ = new NameRow("Ambient","°C");
    colOneLayout->addWidget(ambientTemp_);



    QVBoxLayout* colTwoLayout = new QVBoxLayout();
    colTwoLayout->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    colTwoLayout->setSpacing(0);
    title_ = new QLabel("UI: v1.0 MCU: v" + QString::number(status_.FW_MAJOR_VERSION) + "." + QString::number(status_.FW_MINOR_VERSION));
    colTwoLayout->addWidget(title_);

    for(size_t i = 0; i < status.motorHomeInputArr.size() - 1; i++){
        NameRow* row = new NameRow("Input "+QString::number(i+1), "");
        inputs_.append(row);
        colTwoLayout->addWidget(row);
    }


//    for(size_t i = 0; i < status.motorFaultArr.size() - 1; i++){
//        NameRow* row = new NameRow("Motor Fault "+QString::number(i+1), "");
//        faults_.append(row);
//        colTwoLayout->addWidget(row);
//    }

//    NameRow* modMotor = new NameRow("Mod Fault", "");
//    faults_.append(modMotor);
//    colTwoLayout->addWidget(modMotor);

    for(size_t i = 0; i < status.outputArr.size(); i++){
        QString name = "";
        if(i == OUTPUT::TYPE::COOLING_FAN){
            name = "Fan";
        }else{
            name = "Relay " + QString::number(i);
        }
        NameRow* row = new NameRow(name, "");
        outputs_.append(row);
        colTwoLayout->addWidget(row);
    }


    hLayout_->addLayout(colOneLayout);
    hLayout_->addLayout(colTwoLayout);

    this->setLayout(hLayout_);

    QTimer *timer = new QTimer(this);
    timer->setInterval(500);
    connect(timer, SIGNAL(timeout()), this, SLOT(service()));
    timer->start();
}

void ManualMode::increaseFiringRate()
{
    if(settings_.firingRate < 100){
        settings_.firingRate++;
    }

    if(settings_.firingRate > 5){
        startBurner();
    }

    firingRate_->setValue(settings_.firingRate);
}

void ManualMode::decreaseFiringRate()
{
    if(settings_.firingRate > 0){
        settings_.firingRate--;
    }

    if(settings_.firingRate < 2){
        stopBurner();
    }

    firingRate_->setValue(settings_.firingRate);
}

void ManualMode::startBurner()
{
    if(settings_.flameOn != FLAME::COMMAND::RUN){
        burnerStatus_->setText("Starting");
        settings_.flameOn = FLAME::COMMAND::RUN;
    }
}

void ManualMode::stopBurner()
{
    if(settings_.flameOn != FLAME::COMMAND::STOP){
        burnerStatus_->setText("Stopping");
        settings_.flameOn = FLAME::COMMAND::STOP;
    }
}

void ManualMode::increaseGrill()
{
    if(settings_.grillPosition < 100){
        settings_.grillPosition++;
    }

    grillHeight_->setValue(settings_.grillPosition);
}

void ManualMode::decreaseGrill()
{
    if(settings_.grillPosition > 0){
        settings_.grillPosition--;
    }

    grillHeight_->setValue(settings_.grillPosition);
}

void ManualMode::button1Press()
{
   // qDebug() << "Button 1";
}

void ManualMode::button2Press()
{
   // qDebug() << "Button 2";
}

void ManualMode::service()
{

    title_->setText("UI: v1.0 MCU: v" + QString::number(status_.FW_MAJOR_VERSION) + "." + QString::number(status_.FW_MINOR_VERSION));
    // Update status values
    for(size_t i = 0; i < status_.temperatureArr.size(); i++){
        if(((status_.temperatureArr.at(i)/10) > 1300) || (status_.temperatureArr.at(i) == 0)){
           temperatures_.at(i)->setText("Open");
        }else{
            temperatures_.at(i)->setValue(static_cast<float>(status_.temperatureArr.at(i) / 10.0f));
        }
    }

    for(size_t i = 0; i < status_.motorHomeInputArr.size() - 1; i++){
        inputs_.at(i)->setText(status_.motorHomeInputArr.at(i)? "Open":"Closed" );
    }

//    for(size_t i = 0; i < status_.motorFaultArr.size(); i++){
//        faults_.at(i)->setValue(status_.motorFaultArr.at(i));
//    }

    float count = 0;
    float value = 0;
    for(size_t i = 0; i < status_.tempFaultArr.size(); i++){
        if((status_.tempFaultArr.at(i) == TEMP::FAULT::NONE) && (status_.ambientTempArr.at(i) != 0)
                && (status_.ambientTempArr.at(i) < 1000)){
            count++;
            value += (status_.ambientTempArr.at(i)/10.0f);
        }
    }

    if(count != 0){
        ambientTemp_->setValue(value/count);
    }else{
        ambientTemp_->setText("Fault");
    }

    for(size_t i = 0; i < status_.outputArr.size(); i++){
        QString text = "";
        if(i == OUTPUT::TYPE::COOLING_FAN){
            text = (status_.outputArr.at(i))? "On" : "Off";
        }else{
            text = (status_.outputArr.at(i))? "Closed" : "Open";
        }
        outputs_[i]->setText(text);
    }


    if(status_.flameStatus == FLAME::STATUS::FLAME_ON){
        burnerStatus_->setText("ON");
    }else{
        burnerStatus_->setText("OFF");
    }
}

