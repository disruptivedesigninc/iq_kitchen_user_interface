#include "auto_mode_page.hpp"
#include "defines.hpp"

AutoModePage::AutoModePage(QWidget *parent): PageWidget(parent),
    listView_(),
    builder_(),
    test_()
{
    this->setTitle("Auto Operation");

    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::HEAT_GRILL, 150, 0, 0);
    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::KEEP_PRODUCT, 150, 0, 60*5);
    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::ANNOUNCE, 0, 0, 0);
    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::COOK_PRODUCT, 150, 120, 0);
    test_.addNewSegment();
    test_.updateSegment(SEGMENT::MODE::REST_PRODUCT, 0, 0, 60*5 + 30);


    listView_.setFixedSize(PAGE_WIDTH/3, PAGE_HEIGHT - 50);

    listView_.addListItem("Steak");
    listView_.addListItem("Fish");
    listView_.addListItem("Brisket");


    this->hLayout_.addWidget(&listView_);
    this->hLayout_.addWidget(&builder_);




    connect(&listView_, SIGNAL(itemSelectedSignal(int)), this, SLOT(itemSelectedSlot(int)));
    connect(&listView_, SIGNAL(currentRowChanged(int)), this, SLOT(itemRowChangedSlot(int)));
    connect(&builder_, SIGNAL(itemSelectedSignal(int)), this, SLOT(builderSelectedSlot(int)));
}

void AutoModePage::itemSelectedSlot(int row)
{
    // If selected then open Run View
    listView_.close();
    hLayout_.removeWidget(&listView_);

    emit hideNavigation(true);

    this->setFixedSize(LCD_WIDTH, PAGE_HEIGHT);
}

void AutoModePage::itemRowChangedSlot(int row)
{
    // When changed display information on the other screen.
    builder_.loadSequence(test_);
}

void AutoModePage::builderSelectedSlot(int row)
{

}
