#include "readinputevents.hpp"
#include <unistd.h>
#include <QDebug>

ReadInputEvents::ReadInputEvents():QObject()
{
    if ((fd0 = open("/dev/input/event0", O_RDONLY|O_NONBLOCK)) < 0){
       //qDebug() << "Failed to open event 0";
    }

    if ((fd1 = open("/dev/input/event1", O_RDONLY|O_NONBLOCK)) < 0){
      // qDebug() << "Failed to open event 1";
    }

    if ((fd2 = open("/dev/input/event2", O_RDONLY|O_NONBLOCK)) < 0){
      // qDebug() << "Failed to open event 2";
    }


    QTimer *timer = new QTimer(this);
    timer->setInterval(100);
    connect(timer, &QTimer::timeout, this, &ReadInputEvents::service);
    timer->start();
}

void ReadInputEvents::service()
{

    struct input_event event0;
    while((int)read(fd0, &event0, sizeof(event0)) > 0){
        if(event0.type == 1){
            if(event0.code == 103){
                // Key up - Right button
                if(event0.value == 0){
                    emit button2();
                    emit buttonSoundTrigger();
                }
            }else if(event0.code == 108){
                // Key down - left button
                if(event0.value == 0){
                    emit button1();
                    emit buttonSoundTrigger();
                }
            }
        }
    }


    struct input_event event1;
    while((int)read(fd1, &event1, sizeof(event1)) > 0){
        if(event1.value == 1){
            emit rotary1Up();
            emit rotarySoundTrigger();
        }else if(event1.value == -1){
            emit rotary1Down();
            emit rotarySoundTrigger();
        }
    }


    struct input_event event2;
    while((int)read(fd2, &event2, sizeof(event2)) > 0){
        if(event2.value == 1){
                emit rotary2Up();
                emit rotarySoundTrigger();
        }else if(event2.value == -1){
                emit rotary2Down();
                emit rotarySoundTrigger();

        }
    }


}
