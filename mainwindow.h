#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <QLabel>
#include <QStackedWidget>
#include "pages/manualmode.hpp"
#include "pages/auto_mode_page.hpp"
#include "pages/profile_page.hpp"
#include "pages/setting_page.hpp"
#include "defines.hpp"
#include "navigationbar.hpp"
#include "settings.hpp"
#include "status.hpp"
#include "inter_processor_message.hpp"
#include "pages/readinputevents.hpp"
#include <QSoundEffect>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


public slots:
    void rotarySoundSlot();
    void buttonSoundSlot();

private:

    Settings settings_;
    Status status_;
    InterProcessorMessage comms_;
    ManualMode window_;
    ReadInputEvents inputEvents_;
    QSoundEffect rotaryClick_;



protected:
    bool eventFilter(QObject *obj, QEvent *event) override;

};
#endif // MAINWINDOW_H
