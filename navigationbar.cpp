#include "navigationbar.hpp"
#include "defines.hpp"
#include <QPainter>

NavigationBar::NavigationBar(int width, int height, QWidget *parent) :
    QWidget(parent),
    autoPage_(),
    manualPage_(),
    profilePage_(),
    settingsPage_(),
    currentIndex_(0)
{
    this->setFixedSize(width, height);

    QPixmap autoPageIcon(":/images/images/WebLogo3.png");
    QPixmap manualPageIcon(":/images/images/WebLogo3.png");
    QPixmap profilePageIcon(":/images/images/WebLogo3.png");
    QPixmap settingsPageIcon(":/images/images/WebLogo3.png");

    autoPage_.setPixmap(autoPageIcon);
    manualPage_.setPixmap(manualPageIcon);
    profilePage_.setPixmap(profilePageIcon);
    settingsPage_.setPixmap(settingsPageIcon);


    navigationList_.append(&autoPage_);
    navigationList_.append(&manualPage_);
    navigationList_.append(&profilePage_);
    navigationList_.append(&settingsPage_);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->setAlignment(Qt::AlignTop);
    layout->setSpacing(0);
    layout->setContentsMargins(0,0,0,0);

    foreach( auto& x, navigationList_){
        x->setFixedSize(width, width);
        x->setAlignment(Qt::AlignVCenter);
        layout->addWidget(x);
    }

    setLayout(layout);

    updateSelection(currentIndex_);
}

bool NavigationBar::moveUp()
{
    if(currentIndex_ > 0){
        currentIndex_--;
        updateSelection(currentIndex_);
        return true;
    }
    return false;
}

bool NavigationBar::moveDown()
{
    if(currentIndex_ < (navigationList_.count() - 1)){
        currentIndex_++;
        updateSelection(currentIndex_);
        return true;
    }
    return false;
}

int NavigationBar::currentIndex() const
{
    return currentIndex_;
}

void NavigationBar::hideView(bool hide)
{
    if(hide){
        this->hide();
    }else{
        this->setVisible(true);
    }
}

void NavigationBar::updateSelection(int index)
{
    // Clear other selections
    foreach( auto& x, navigationList_){
        x->setStyleSheet("background-color:transparent;");
    }

    // highlight this selection
    navigationList_.at(index)->setStyleSheet("background-color:#4BC3CF;");
}

void NavigationBar::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    QWidget::paintEvent(event);
}
