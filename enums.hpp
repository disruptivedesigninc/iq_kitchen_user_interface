/*
 * enums.hpp
 *
 *  Created on: Jan. 18, 2021
 *      Author: rbaro
 */
#pragma once
#include <stdint.h>


namespace MOTOR {
enum HOME_DIRECTION {CW = 0, CCW};
enum FAULT : uint8_t {NONE = 0, MOTOR, HOME_SWITCH};
enum STATE : uint8_t {FAULT = 0, INITIALIZE, HOMING, MOVING_TO_POSITION, AT_POSITION};
enum PWM_NUMBER {ONE = 0, TWO, THREE, FOUR, FIVE, TOTAL_NUM};

}

namespace TEMP {
enum TEMP_NUMBER : uint8_t {ONE = 0, TWO, THREE, FOUR, FIVE, SIX, TOTAL_NUM};
enum FAULT : uint8_t {NONE = 0, OPEN, SHORT};
enum STATE : uint8_t {READY = 0, READING};
enum APPLICATION : uint8_t {GRILL = TEMP_NUMBER::ONE, PRODUCT = TEMP_NUMBER::TWO };
}

namespace FLAME {
enum COMMAND : uint8_t { STOP = 0x00, RUN = 0x6C}; // Used for flameOn in settings
enum HEAT : uint8_t { NONE = 0x00, CALL = 0xA9};   // Used for call for heat in settings
enum STATUS : uint8_t { FLAME_OFF = 0x00, FLAME_ON = 0x11}; // Used for flame status in status
}

namespace COMMS {
static const uint32_t PREAMBLE = 0xACF6;
static const uint32_t PREAMBLE_SIZE = sizeof(PREAMBLE);
static const uint32_t CRC_SIZE = sizeof(uint32_t);
}

namespace SEGMENT {
enum class MODE : uint8_t {STOP = 0, HEAT_GRILL, COOK_PRODUCT, KEEP_PRODUCT, KEEP_WARM, REST_PRODUCT, ANNOUNCE, MANUAL};
enum class COMMAND : uint8_t {OFF = 0, PAUSE, RUN};
}

namespace OUTPUT{
enum TYPE {COOLING_FAN = 0, RELAY_1, RELAY_2, RELAY_3, RELAY_4, NUM_OUTPUTS};
enum STATE{OFF = 0, ON = 1};
}
