#include "inter_processor_message.hpp"
#include <QByteArray>
#include <array>
#include <QDebug>


InterProcessorMessage::InterProcessorMessage(Settings& settings, Status& status): QObject(),
    port_("/dev/ttyRPMSG0"),
    settings_(settings),
    status_(status),
    writeCycleTimer_(this),
    errorCount_(0),
    readSuccessCount_(0)
{
    port_.open(QIODevice::ReadWrite);
    port_.flush();

    connect(&port_, SIGNAL(readyRead()), this, SLOT(read()));
     //QTimer::singleShot(READ_CHECK_MS, this, SLOT(read()));
    connect(&writeCycleTimer_, &QTimer::timeout, this, &InterProcessorMessage::write);
    writeCycleTimer_.start(WRITE_CYCLE_MS);
}

void InterProcessorMessage::read()
{

    if(port_.bytesAvailable()){

        QByteArray array = port_.readAll();
                    //qDebug() << "Receive Status hex:" << array.toHex();
                    status_.unpack((uint8_t*)array.data());
                    readSuccessCount_++;
    }
    //QTimer::singleShot(READ_CHECK_MS, this, SLOT(read()));
}

void InterProcessorMessage::write()
{


        static std::array<uint8_t, settingPacketSize()> buffer;
        buffer.fill(0);
        settings_.pack(buffer.data());

        QByteArray array = QByteArray::fromRawData((char*)buffer.data(), buffer.size());
        port_.write(array);
}



