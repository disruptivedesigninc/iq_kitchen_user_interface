#pragma once

#include <QWidget>
#include <QSpinBox>
#include <QPaintEvent>
#include <QStyleOption>

class SpinBox : public QSpinBox
{
    Q_OBJECT
public:
    explicit SpinBox(QWidget *parent = nullptr);

signals:

public slots:
    void increment();
    void decrement();

private:

protected:
    void paintEvent(QPaintEvent *event) override;
    void focusOutEvent(QFocusEvent* event) override;

};


