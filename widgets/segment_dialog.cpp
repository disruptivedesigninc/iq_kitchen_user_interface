#include "segment_dialog.hpp"
#include "defines.hpp"
#include <QLabel>
#include <QHBoxLayout>
#include "profile/sequence_handler.hpp"
#include <QDebug>


SegmentDialog::SegmentDialog(ProfileSequence::Segment &segment, QWidget *parent) : QDialog(parent),
    segment_(segment),
    layout(),
    navigationList_(),
    currentIndex_(0),
    mode_("Mode: "),
    descriptionText_(),
    grillSp_(nullptr),
    productSp_(nullptr),
    time_(nullptr),
    grillPos_(nullptr),
    firingRatePos_(nullptr),
    subLayout_()
{

    this->setModal(true);

    this->setFixedSize(LCD_WIDTH, LCD_HEIGHT);

    QLabel* title = new QLabel("Segment");

    layout.setSpacing(0);
    subLayout_.setSpacing(0);
    subLayout_.setAlignment(Qt::AlignTop);
    layout.setAlignment(Qt::AlignTop);

    layout.addWidget(title);

    // Add Mode
    mode_.comboBox()->addItems(SequenceHandler::SEGMENT_STR);
    mode_.setFixedHeight(ROW_HEIGHT);
    layout.addWidget(&mode_);
    layout.addWidget(&descriptionText_);

    layout.addLayout(&subLayout_);
    this->setLayout(&layout);

    mode_.comboBox()->setCurrentIndex((int)segment_.mode);
    mode_.installEventFilter(this);
    navigationList_.append(&mode_);

    buildSettings();
    connect(mode_.comboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(modeChangedSlot(int)));
}

void SegmentDialog::grillSpSlot(int setpoint)
{
    segment_.grillTemp = setpoint;
}

void SegmentDialog::productSpSlot(int setpoint)
{
    segment_.productTemp = setpoint;
}

void SegmentDialog::timeSlot(uint16_t time)
{
    segment_.time = time;
}

void SegmentDialog::grillPosSlot(int position)
{
    segment_.grillTemp = position;
}

void SegmentDialog::firingRateSlot(int rate)
{
    segment_.productTemp = rate;
}

void SegmentDialog::modeChangedSlot(int index)
{
    segment_.mode = static_cast<SEGMENT::MODE>(index);
    buildSettings();
}

void SegmentDialog::buildSettings()
{
    clearLayout();

    switch(segment_.mode){
        case SEGMENT::MODE::STOP:{
            descriptionText_.setText("Stop the burner and position the grill to 0%");
        break;
        }
        case SEGMENT::MODE::HEAT_GRILL:{
        //Grill temp, burner on
            descriptionText_.setText("Heat the grill to the grill setpoint.");
            addGrillTempSp();
            break;
        }
        case SEGMENT::MODE::COOK_PRODUCT:{
        //Grill temp, product temp, burner on
        descriptionText_.setText("Maintain the grill setpoint temperature, and heat the product to its setpoint.");
            addGrillTempSp();
            addProductTempSP();
            break;
        }
        case SEGMENT::MODE::KEEP_PRODUCT:{
        //Grill temp, Time, burner on
        descriptionText_.setText("Maintain the grill setpoint temperature for the set time.");
            addGrillTempSp();
            addTimeSp();
            break;
        }
        case SEGMENT::MODE::KEEP_WARM:{
        //Grill temp, time, stop burner after
        descriptionText_.setText("Maintain the grill setpoint temperature for the set time, then turn burner off.");
            addGrillTempSp();
            addTimeSp();
            break;
        }
        case SEGMENT::MODE::REST_PRODUCT:{
        //burner off, grill 100%, time
        descriptionText_.setText("Turn the burner off and raise the grill to 100% for the set time");
            addTimeSp();
            break;
        }
        case SEGMENT::MODE::ANNOUNCE:{
        // minimum firing rate, grill 100%, timeout - burner off.
        descriptionText_.setText("Announce cooking stage is complete. Burner will go to minimum, and grill to maximum position");
            break;
        }
        case SEGMENT::MODE::MANUAL:{
        // firing rate, grill sp, time, time == 0 hold forever, else burner off.
        descriptionText_.setText("Manually set grill position, burner firing rate and time. If time is 0 it will not elapse.");
            addGrillPos();
            addFiringRate();
            addTimeSp();
            break;
        }
    }
}

void SegmentDialog::clearLayout()
{
    // Hacky way to rebuild nav list
    navigationList_.clear();
    navigationList_.append(&mode_);
    currentIndex_ = 0;

    while(QLayoutItem* item = subLayout_.takeAt(0)){
        if(QWidget* widget = item->widget()){
            widget->disconnect();
            delete widget;
            widget = nullptr;
        }
        delete item;
    }
}

void SegmentDialog::addGrillTempSp()
{
    grillSp_ = new SettingSpinBox("Grill Temperature Setpoint");
    grillSp_->spinBox()->setRange(MIN_TEMP, MAX_TEMP);
    grillSp_->spinBox()->setValue(segment_.grillTemp);
    grillSp_->setFixedHeight(ROW_HEIGHT);
    navigationList_.append(grillSp_);
    subLayout_.addWidget(grillSp_);
    connect(grillSp_->spinBox(), QOverload<int>::of(&QSpinBox::valueChanged),
            this, &SegmentDialog::grillSpSlot);
}

void SegmentDialog::addProductTempSP()
{
    productSp_ = new SettingSpinBox("Product Temperature Setpoint");
    productSp_->spinBox()->setRange(MIN_TEMP, MAX_TEMP);
    productSp_->spinBox()->setValue(segment_.productTemp);
    productSp_->setFixedHeight(ROW_HEIGHT);
    navigationList_.append(productSp_);
    subLayout_.addWidget(productSp_);
    connect(productSp_->spinBox(), QOverload<int>::of(&QSpinBox::valueChanged),
            this, &SegmentDialog::productSpSlot);
}

void SegmentDialog::addTimeSp()
{
    time_ = new SettingTimeBox("Cook Time");
    time_->setInitialTimeSeconds(segment_.time);
    time_->setFixedHeight(ROW_HEIGHT);
    navigationList_.append(time_);
    subLayout_.addWidget(time_);
    connect(time_, SIGNAL(timeBoxTimeSecondsSignal(uint16_t)), this, SLOT(timeSlot(uint16_t)));
}

void SegmentDialog::addGrillPos()
{
    grillPos_ = new SettingSpinBox("Grill Position:");
    grillPos_->spinBox()->setRange(MIN_POSITION, MAX_POSITION);
    grillPos_->spinBox()->setValue(segment_.grillTemp);
    grillPos_->setFixedHeight(ROW_HEIGHT);
    navigationList_.append(grillPos_);
    subLayout_.addWidget(grillPos_);
    connect(grillPos_->spinBox(), QOverload<int>::of(&QSpinBox::valueChanged),
            this, &SegmentDialog::grillPosSlot);
}

void SegmentDialog::addFiringRate()
{
    firingRatePos_ = new SettingSpinBox("Firing Rate:");
    firingRatePos_->spinBox()->setRange(MIN_POSITION, MAX_POSITION);
    firingRatePos_->spinBox()->setValue(segment_.productTemp);
    firingRatePos_->setFixedHeight(ROW_HEIGHT);
    navigationList_.append(firingRatePos_);
    subLayout_.addWidget(firingRatePos_);
    connect(firingRatePos_->spinBox(), QOverload<int>::of(&QSpinBox::valueChanged),
            this, &SegmentDialog::firingRateSlot);
}



void SegmentDialog::focusInEvent(QFocusEvent *)
{
     mode_.setFocus();
}

void SegmentDialog::keyPressEvent(QKeyEvent *event)
{
    if(event->type() == QEvent::KeyPress){
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        switch(keyEvent->key()){
        case Qt::Key::Key_Up:
            nextWidget();
            break;
        case Qt::Key::Key_Down:
            previousWidget();
            break;
        case Qt::Key::Key_Right:
            increment();
            break;
        case Qt::Key::Key_Left:
            decrement();
            break;
        case Qt::Key::Key_P:
            actionSelect();
            break;
        case Qt::Key::Key_Return:
            this->accept();
            break;
        default:
            break;
        }
    }
}

void SegmentDialog::nextWidget()
{
       qDebug() << "Dialog Next";
       if(currentIndex_ > 0){
           currentIndex_--;
           navigationList_.at(currentIndex_)->setFocus();
       }
}

void SegmentDialog::previousWidget()
{
    qDebug() << "Dialog Previous";
    if(currentIndex_ < (navigationList_.count() - 1)){
        currentIndex_++;
        navigationList_.at(currentIndex_)->setFocus();
    }
}

void SegmentDialog::increment()
{
    qDebug() << "Dialog Increment";
    if(auto*w = dynamic_cast<SettingSpinBox*>(navigationList_.at(currentIndex_))){
        w->spinBox()->increment();
    }else if(auto*w = dynamic_cast<SettingComboBox*>(navigationList_.at(currentIndex_))){
        w->comboBox()->increment();
    }else if(auto*w = dynamic_cast<SettingTimeBox*>(navigationList_.at(currentIndex_))){
        w->increment();
    }
}

void SegmentDialog::decrement()
{
    qDebug() << "Dialog Decrement";
    if(auto*w = dynamic_cast<SettingSpinBox*>(navigationList_.at(currentIndex_))){
        w->spinBox()->decrement();
    }else if(auto*w = dynamic_cast<SettingComboBox*>(navigationList_.at(currentIndex_))){
        w->comboBox()->decrement();
    }else if(auto*w = dynamic_cast<SettingTimeBox*>(navigationList_.at(currentIndex_))){
        w->decrement();
    }
}

void SegmentDialog::actionSelect()
{

}
