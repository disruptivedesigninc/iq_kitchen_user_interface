#pragma once

#include <QWidget>
#include <QDialog>
#include <QEvent>
#include <QKeyEvent>
#include <QVBoxLayout>
#include "widgets/setting_spinbox.hpp"
#include "profile/profile_sequence.hpp"
#include "widgets/setting_combobox.hpp"
#include "widgets/setting_timebox.hpp"

class SegmentDialog : public QDialog
{
    Q_OBJECT
public:
    explicit SegmentDialog(ProfileSequence::Segment& segment, QWidget *parent = nullptr);

signals:
    void returnNavigation();

private slots:
    void grillSpSlot(int setpoint);
    void productSpSlot(int setpoint);
    void timeSlot(uint16_t time);
    void grillPosSlot(int position);
    void firingRateSlot(int rate);
    void modeChangedSlot(int index);

private:
    ProfileSequence::Segment& segment_;
    QVBoxLayout layout;
    QList<QWidget*> navigationList_;
    int currentIndex_;
    SettingComboBox mode_;
    QLabel descriptionText_;
    SettingSpinBox* grillSp_;
    SettingSpinBox* productSp_;
    SettingTimeBox* time_;
    SettingSpinBox* grillPos_;
    SettingSpinBox* firingRatePos_;
    QVBoxLayout subLayout_;

    void buildSettings();

    void clearLayout();

    void addGrillTempSp();
    void addProductTempSP();
    void addTimeSp();
    void addGrillPos();
    void addFiringRate();

    static constexpr int ROW_HEIGHT = 60;
    static constexpr int MIN_TEMP = 0;
    static constexpr int MAX_TEMP = 1000;
    static constexpr int MIN_POSITION = 0;
    static constexpr int MAX_POSITION = 100;


protected:
    void focusInEvent(QFocusEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;


    void nextWidget();
    void previousWidget();
    void increment();
    void decrement();
    void actionSelect();

};


