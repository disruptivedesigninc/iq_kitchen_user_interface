#pragma once

#include <QWidget>
#include <QLabel>
#include <QString>
#include <QHBoxLayout>
#include "widgets/combobox.hpp"

class SettingComboBox : public QWidget
{
    Q_OBJECT
public:
    explicit SettingComboBox(const QString& name, QWidget *parent = nullptr);

    ComboBox* comboBox(){return &comboBox_;}

private:

    QLabel label_;
    ComboBox comboBox_;
    QHBoxLayout layout_;

protected:
    void focusInEvent(QFocusEvent* event) override;
    void focusOutEvent(QFocusEvent* event) override;
    void paintEvent(QPaintEvent *event) override;

};

