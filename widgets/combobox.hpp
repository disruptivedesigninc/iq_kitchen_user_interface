#pragma once

#include <QWidget>
#include <QComboBox>
#include <QPaintEvent>
#include <QStyleOption>

class ComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit ComboBox(QWidget *parent = nullptr);



public slots:
    void increment();
    void decrement();


};


