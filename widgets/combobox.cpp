#include "combobox.hpp"
#include <QPainter>
#include <QDebug>

ComboBox::ComboBox(QWidget *parent) : QComboBox(parent)
{


}

void ComboBox::increment()
{
    if(this->currentIndex() > 0){
        this->setCurrentIndex(this->currentIndex()-1);
    }

}

void ComboBox::decrement()
{
    if(this->currentIndex() < this->count() - 1){
        this->setCurrentIndex(this->currentIndex()+1);
    }
}

