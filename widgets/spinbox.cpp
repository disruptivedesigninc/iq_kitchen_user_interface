#include "spinbox.hpp"
#include <QPainter>
#include <QLineEdit>


SpinBox::SpinBox(QWidget *parent) : QSpinBox(parent)
{
    this->setButtonSymbols(QAbstractSpinBox::NoButtons);
    this->setStyleSheet(
        "QSpinBox {"
        "border-width: 0px"
        "border-color: transparent"
        "background-color: transparent"
        "height: 1100"
        "font.pixelSize: 100"
        "}"
                );
    this->setReadOnly(true);
}

void SpinBox::increment()
{
    this->stepUp();
    this->lineEdit()->deselect();
}

void SpinBox::decrement()
{
    this->stepDown();
    this->lineEdit()->deselect();
}

void SpinBox::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    QWidget::paintEvent(event);
}

void SpinBox::focusOutEvent(QFocusEvent *event)
{
        this->lineEdit()->deselect();
}
