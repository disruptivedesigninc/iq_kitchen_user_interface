#include "name_row.hpp"

NameRow::NameRow(const QString& name, QString suffix, QWidget *parent) : QWidget(parent),
    suffix_(suffix)
{
    this->setFixedSize(400,60);
    name_ = new QLabel(name + ":  ");
    value_ = new QLabel("0 " + suffix);

    name_->setFixedSize(280, 50);
    value_->setFixedSize(120, 50);

    name_->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    value_->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

    QHBoxLayout* layout = new QHBoxLayout();
    layout->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    layout->setSpacing(1);
    layout->addWidget(name_);
    layout->addWidget(value_);
    this->setLayout(layout);
}
