#pragma once

#include <QWidget>
#include <QLabel>
#include "spinbox.hpp"
#include <QVBoxLayout>
#include <QString>

class ControlBox : public QWidget
{
    Q_OBJECT
public:
    explicit ControlBox(const QString &title, int width, int height, QWidget *parent = nullptr);

    void increment();
    void decrement();

    SpinBox& spinBox() { return spinBox_;}

private:
    QLabel title_;
    SpinBox spinBox_;
    QVBoxLayout layout_;

protected:
    void focusInEvent(QFocusEvent* event) override;
    void focusOutEvent(QFocusEvent* event) override;
    void paintEvent(QPaintEvent *event) override;


};


