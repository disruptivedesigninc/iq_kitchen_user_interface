#pragma once

#include <QWidget>
#include "widgets/list_view.hpp"
#include "profile/profile_sequence.hpp"

class BuilderList : public ListView
{
    Q_OBJECT
public:
    explicit BuilderList(QWidget *parent = nullptr);

    void loadSequence(ProfileSequence& sequence);
    void buildView();

    ProfileSequence &sequence();

signals:

private:
    ProfileSequence sequence_;

    void appendAddButton();

    QString segmentListStr();

};


