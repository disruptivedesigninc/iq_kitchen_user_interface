#include "builder_list.hpp"
#include "Util.hpp"
#include "profile/sequence_handler.hpp"

BuilderList::BuilderList(QWidget *parent) : ListView(parent), sequence_()
{

}

void BuilderList::loadSequence(ProfileSequence &sequence)
{
    sequence_ = sequence;
    buildView();
}

ProfileSequence &BuilderList::sequence()
{
    return sequence_;
}

void BuilderList::buildView()
{
    this->clear();
    sequence_.firstSegment();


    bool nextSegment = !sequence_.isEmpty();
    while(nextSegment){
        this->addListItem(segmentListStr());
        nextSegment = sequence_.nextSegment();
    }

    appendAddButton();
}

void BuilderList::appendAddButton()
{
    this->addListItem("+ Add");
}

QString BuilderList::segmentListStr()
{
    using namespace UTIL;
    auto& segment = sequence_.currentSegment();
    QString str;
    const QChar bullet = (8226);
    switch(segment.mode){
        case SEGMENT::MODE::STOP:{
        str =  QString(bullet) + " Stop";
        break;
        }
        case SEGMENT::MODE::HEAT_GRILL:{
        str =QString(bullet) +  " Heat grill to " + displayTemp(segment.grillTemp);
            break;
        }
        case SEGMENT::MODE::COOK_PRODUCT:{
        str =QString(bullet) +  " Heat product to " + displayTemp(segment.productTemp) + " at " + displayTemp(segment.grillTemp) + " grill temp.";
            break;
        }
        case SEGMENT::MODE::KEEP_PRODUCT:{
        str =QString(bullet) +  " Heat product for " + displayTimefromSecLong(segment.time) + " at " + displayTemp(segment.grillTemp) + " grill temp.";
            break;
        }
        case SEGMENT::MODE::KEEP_WARM:{
        str =QString(bullet) +  " Keep warm for " + displayTimefromSecLong(segment.time) + " at " + displayTemp(segment.grillTemp) + " grill temp then turn off";
            break;
        }
        case SEGMENT::MODE::REST_PRODUCT:{
        str =QString(bullet) +  " Turn grill off and rest product for " + displayTimefromSecLong(segment.time);
            break;
        }
        case SEGMENT::MODE::ANNOUNCE:{
            if(sequence_.peekLastSegment().mode != SEGMENT::MODE::STOP){
                str = QString(bullet) + " Announce " + SequenceHandler::SEGMENT_STR.at((int)sequence_.peekLastSegment().mode) + " is complete and wait until acknowledged.";
            }else{
              str = QString(bullet) + " Announce stage complete";
            }

            break;
        }
        case SEGMENT::MODE::MANUAL:{
         str = QString(bullet) + " Heat at " + QString::number(segment.productTemp) +"% with " +QString::number(segment.grillTemp) + "% grill position for " + displayTemp(segment.grillTemp);
            break;
        }
    }

    return str;
}
