#include "setting_spinbox.hpp"
#include <QPainter>
#include <QPaintEvent>
#include <QStyleOption>
#include <QLineEdit>

SettingSpinBox::SettingSpinBox(const QString &name, QWidget *parent) : QWidget(parent),
    label_(name), spinBox_(), layout_()
{
    layout_.setSpacing(2);
    layout_.addWidget(&label_);
    layout_.addWidget(&spinBox_);
    this->setLayout(&layout_);
}

void SettingSpinBox::focusInEvent(QFocusEvent *)
{
    this->setStyleSheet("background-color:#4BC3CF;");
}

void SettingSpinBox::focusOutEvent(QFocusEvent *)
{
    this->setStyleSheet("background-color:transparent;");
}

void SettingSpinBox::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    QWidget::paintEvent(event);
}


