#pragma once

#include <QObject>
#include <QListWidget>
#include <QListWidgetItem>
#include <QVector>

class ListView : public QListWidget
{
    Q_OBJECT
public:
    explicit ListView(QWidget *parent = nullptr);


    void addListItem(const QString& name);

    void increment();
    void decrement();
    void actionSelect();

signals:
    void itemSelectedSignal(int index);



protected:
    //void paintEvent(QPaintEvent *event) override;
    void focusInEvent(QFocusEvent* event) override;
    void focusOutEvent(QFocusEvent* event) override;


};


