#pragma once

#include <QWidget>
#include <QLabel>
#include <QString>
#include <QHBoxLayout>
#include <QTime>

class SettingTimeBox : public QWidget
{
    Q_OBJECT
public:
    explicit SettingTimeBox(const QString &name, QWidget *parent = nullptr);


    void setInitialTimeSeconds(uint16_t seconds);

signals:
    void timeBoxTimeSecondsSignal(uint16_t seconds);

public slots:
    void increment();
    void decrement();

private:
    QLabel label_;
    QLabel timeLabel_;
    QHBoxLayout layout_;
    QTime time_;

    void updateTimeLabel();
    void sendNewTime();

    static constexpr int TIME_STEP_SEC = 60;
    static constexpr int TIME_STEP_MIN = 1;
    static constexpr int TIME_STEP_HOUR = 1;
    static constexpr int MAX_TIME_SEC = 16*60*60;
    static constexpr int MIN_TIME_SEC = 0;


protected:
    void focusInEvent(QFocusEvent* event) override;
    void focusOutEvent(QFocusEvent* event) override;
    void paintEvent(QPaintEvent *event) override;

};


