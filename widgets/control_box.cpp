#include "control_box.hpp"
#include <QPainter>
#include <QPaintEvent>
#include <QStyleOption>

ControlBox::ControlBox(const QString &title, int width, int height, QWidget *parent) :
    QWidget(parent),
    title_(title, this),
    spinBox_(this),
    layout_()
{
    this->setStyleSheet("background-color:transparent;");
    spinBox_.setReadOnly(true);
    this->setFixedSize(width, height);
    layout_.setSpacing(15);
    layout_.setAlignment(Qt::AlignHCenter);
    layout_.addWidget(&spinBox_);
    layout_.addWidget(&spinBox_);
    this->setLayout(&layout_);
}

void ControlBox::increment()
{
    spinBox_.increment();
}

void ControlBox::decrement()
{
    spinBox_.decrement();
}

void ControlBox::focusInEvent(QFocusEvent *)
{
    this->setStyleSheet("background-color:#4BC3CF;");
}

void ControlBox::focusOutEvent(QFocusEvent *)
{
    this->setStyleSheet("background-color:transparent;");
}

void ControlBox::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    QWidget::paintEvent(event);
}

