#pragma once
#include <QString>
#include <QTime>


static bool isFahrenheit = false;

namespace UTIL{

QString displayTemp(float cTemp){
    int temp = static_cast<int>((isFahrenheit)? ((cTemp*9/5)+32): cTemp);
    const QChar degree = (0260);
    return QString::number(temp) + degree + ((isFahrenheit)? "F":"C");
}

QString displayTimefromSecShort(uint16_t seconds){
    uint16_t hours = seconds / (60*60);
    uint16_t minutes = (seconds % (60*60)) / 60;
    uint16_t secs = (seconds % 60) / 60;

    QTime time = QTime(hours, minutes, secs);

    return time.toString("HH:MM:SS");
}

QString displayTimefromSecLong(uint16_t seconds){
    uint16_t hours = seconds / (60*60);
    uint16_t minutes = (seconds % (60*60)) / 60;
    uint16_t secs = (seconds % 60) / 60;

    QString time;
    if(hours > 0){
        time += QString::number(hours) + " hours ";
    }

    if(minutes > 0){
        time += QString::number(minutes) + " minutes ";
    }

    if(secs > 0){
        time +=QString::number(secs) + " seconds";
    }

    return time;
}

float FtoC(float fTemp){
    return (fTemp - 32) * 5/9;
}

}

