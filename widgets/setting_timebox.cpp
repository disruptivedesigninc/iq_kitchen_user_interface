#include "setting_timebox.hpp"

#include <QPainter>
#include <QPaintEvent>
#include <QStyleOption>

SettingTimeBox::SettingTimeBox(const QString& name, QWidget *parent) : QWidget(parent),
    label_(name),
    timeLabel_(),
    layout_(),
    time_(0,0)
{
    layout_.setSpacing(2);
    layout_.addWidget(&label_);
    layout_.addWidget(&timeLabel_);
    this->setLayout(&layout_);
    updateTimeLabel();
}

void SettingTimeBox::setInitialTimeSeconds(uint16_t seconds)
{
    if(seconds > MAX_TIME_SEC){
        seconds = MAX_TIME_SEC;
    }
    QTime base = QTime(0,0);
    time_ = base.addSecs(seconds);
    updateTimeLabel();
}

void SettingTimeBox::increment()
{
    if(time_.addSecs(TIME_STEP_SEC) < QTime(0,0).addSecs(MAX_TIME_SEC)){
        time_ = time_.addSecs(TIME_STEP_SEC);
        updateTimeLabel();
        sendNewTime();
    }

}

void SettingTimeBox::decrement()
{
    if(time_.addSecs(-1*TIME_STEP_SEC) < QTime(0,0).addSecs(MAX_TIME_SEC)){
        time_ = time_.addSecs(-1*TIME_STEP_SEC);
    }else{
        time_ = QTime(0,0);
    }
    updateTimeLabel();
    sendNewTime();
}

void SettingTimeBox::updateTimeLabel()
{
    QString timeStr;

    if(time_.hour() > 1){
        timeStr = time_.toString("H") + " Hours ";
    }else if(time_.hour() > 0){
       timeStr = time_.toString("H") + " Hour ";
    }

    timeStr += time_.toString("m") + " Minutes";

    timeLabel_.setText(timeStr);
}

void SettingTimeBox::sendNewTime()
{

    uint16_t time = (time_.hour() * 60 * 60) + (time_.minute() * 60) + time_.second();
    emit timeBoxTimeSecondsSignal(time);
}

void SettingTimeBox::focusInEvent(QFocusEvent *)
{
    this->setStyleSheet("background-color:#4BC3CF;");
}

void SettingTimeBox::focusOutEvent(QFocusEvent *)
{
    this->setStyleSheet("background-color:transparent;");
}

void SettingTimeBox::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    QWidget::paintEvent(event);
}
