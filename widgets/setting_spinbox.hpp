#pragma once

#include <QWidget>
#include "spinbox.hpp"
#include <QLabel>
#include <QString>
#include <QHBoxLayout>

class SettingSpinBox : public QWidget
{
    Q_OBJECT
public:
    explicit SettingSpinBox(const QString& name, QWidget *parent = nullptr);

    SpinBox* spinBox() {
        return &spinBox_;
    }

private:

    QLabel label_;
    SpinBox spinBox_;
    QHBoxLayout layout_;

protected:
    void focusInEvent(QFocusEvent* event) override;
    void focusOutEvent(QFocusEvent* event) override;
    void paintEvent(QPaintEvent *event) override;

};


