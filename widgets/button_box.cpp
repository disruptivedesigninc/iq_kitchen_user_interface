#include "button_box.hpp"
#include <QPainter>
#include <QPaintEvent>
#include <QStyleOption>
#include "defines.hpp"

ButtonBox::ButtonBox(int width, int height, QWidget *parent) : QWidget(parent),
    layout_(),
    navigationList_(),
    currentIndex_(0)
{
    this->setFixedSize(width, height);

    layout_.setSpacing(0);
    layout_.setAlignment(Qt::AlignTop | Qt::AlignHCenter);

    this->setLayout(&layout_);

}

void ButtonBox::increment()
{
    if(currentIndex_ > 0){
        currentIndex_--;
        for(int i = 0; i < navigationList_.count(); i++){
            if(i == currentIndex_){
                navigationList_.at(i)->setStyleSheet("background-color:#4BC3CF;");
            }else{
                navigationList_.at(i)->setStyleSheet("background-color:transparent;");
            }
        }
    }
}

void ButtonBox::decrement()
{
    if(currentIndex_ < (navigationList_.count() - 1)){
        currentIndex_++;
        for(int i = 0; i < navigationList_.count(); i++){
            if(i == currentIndex_){
                navigationList_.at(i)->setStyleSheet("background-color:#4BC3CF;");
            }else{
                navigationList_.at(i)->setStyleSheet("background-color:transparent;");
            }
        }
    }
}

void ButtonBox::actionSelect()
{
    emit buttonPressed(currentIndex_);
}

void ButtonBox::addButton(const QString &name)
{
    QLabel* label = new QLabel(name, this);
    label->setFixedSize(this->width(), this->width());

    layout_.addWidget(label);

    navigationList_.append(label);
}

void ButtonBox::focusInEvent(QFocusEvent *)
{
    currentIndex_ = 0;

    for(int i = 0; i < navigationList_.count(); i++){
        if(i == currentIndex_){
            navigationList_.at(i)->setStyleSheet("background-color:#4BC3CF;");
        }else{
            navigationList_.at(i)->setStyleSheet("background-color:transparent;");
        }
    }
}

void ButtonBox::focusOutEvent(QFocusEvent *)
{
    currentIndex_ = 0;
    for(int i = 0; i < navigationList_.count(); i++){
            navigationList_.at(i)->setStyleSheet("background-color:transparent;");
    }
}

void ButtonBox::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    QWidget::paintEvent(event);
}
