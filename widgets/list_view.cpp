#include "list_view.hpp"
#include <QPainter>

ListView::ListView(QWidget *parent) : QListWidget(parent)
{

}

void ListView::addListItem(const QString &name)
{
    this->addItem(name);
}

void ListView::increment()
{

    if( 0 < this->currentRow()){
        this->setCurrentRow(this->currentRow()-1);
    }
}

void ListView::decrement()
{
    if(this->currentRow() + 1 < this->count()){
        this->setCurrentRow(this->currentRow() + 1);
    }
}

void ListView::actionSelect()
{
    emit itemSelectedSignal(this->currentRow());
}

void ListView::focusInEvent(QFocusEvent *)
{
    if(this->count() > 0){
        this->setCurrentRow(0);
    }
}

void ListView::focusOutEvent(QFocusEvent *)
{
    this->clearSelection();
}

//void ListView::paintEvent(QPaintEvent *event)
//{

//    QStyleOption opt;
//    opt.initFrom(this);
//    QPainter p(this);
//    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
//    QWidget::paintEvent(event);

//}
