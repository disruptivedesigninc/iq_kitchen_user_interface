#pragma once

#include <QWidget>
#include <QLabel>
#include "spinbox.hpp"
#include <QVBoxLayout>
#include <QString>
#include "settings.hpp"

class ButtonBox : public QWidget
{
    Q_OBJECT
public:
    explicit ButtonBox(int width, int height, QWidget *parent = nullptr);

    void increment();
    void decrement();
    void actionSelect();

    void addButton(const QString& name);

signals:
    void buttonPressed(int index);


private:
    QVBoxLayout layout_;
    QList<QLabel*> navigationList_;
    int currentIndex_;



protected:
    void focusInEvent(QFocusEvent* event) override;
    void focusOutEvent(QFocusEvent* event) override;
    void paintEvent(QPaintEvent *event) override;

};


