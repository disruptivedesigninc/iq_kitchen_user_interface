#include "setting_combobox.hpp"
#include <QPainter>
#include <QPaintEvent>
#include <QStyleOption>
#include <QDebug>

SettingComboBox::SettingComboBox(const QString &name, QWidget *parent) : QWidget(parent),
    label_(name), comboBox_(), layout_()
{
    layout_.setSpacing(2);
    layout_.addWidget(&label_);
    layout_.addWidget(&comboBox_);
    this->setLayout(&layout_);
}

void SettingComboBox::focusInEvent(QFocusEvent *)
{
    this->setStyleSheet("background-color:#4BC3CF;");
}

void SettingComboBox::focusOutEvent(QFocusEvent *)
{
    this->setStyleSheet("");
}

void SettingComboBox::paintEvent(QPaintEvent *event)
{
    QStyleOption opt;
    opt.initFrom(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
    QWidget::paintEvent(event);
}
