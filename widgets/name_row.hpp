#pragma once

#include <QWidget>
#include <QHBoxLayout>
#include <QString>
#include <QLabel>

class NameRow : public QWidget
{
    Q_OBJECT
public:
    explicit NameRow(const QString& name, QString suffix, QWidget *parent = nullptr);

    void setValue(int value){
        value_->setText(QString::number(value) + " " + suffix_);
    }

    void setValue(float value){
        value_->setText(QString::number(static_cast<int>(value)) + " " + suffix_);
    }

    void setText(const QString& string){
        value_->setText(string);
    }

private:
    QLabel* name_;
    QLabel* value_;
    QString suffix_;
};


