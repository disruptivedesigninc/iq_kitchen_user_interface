#include "profile_sequence.hpp"

ProfileSequence::ProfileSequence(): name_("Program"),sequence_(), currentIndex_(0)
{
    sequence_.reserve(MAX_SEGMENT);
}

bool ProfileSequence::nextSegment()
{
    if(currentIndex_< sequence_.count() - 1){
        currentIndex_++;
        return true;
    }
    return false;
}

bool ProfileSequence::previousSegment()
{
    if(currentIndex_ > 0){
        currentIndex_--;
        return true;
    }
    return false;
}

void ProfileSequence::firstSegment()
{
    currentIndex_ = 0;
}

bool ProfileSequence::moveToSegment(int index)
{
    if(index < sequence_.count()){
        currentIndex_ = index;
        return true;
    }
    return false;
}

bool ProfileSequence::isEmpty() const
{
    return sequence_.isEmpty();
}

bool ProfileSequence::addNewSegment()
{
    if(sequence_.count() < MAX_SEGMENT){
        if(sequence_.count() == 0){
            sequence_.prepend(Segment());
            currentIndex_ = 0;
        }else if( currentIndex_ == sequence_.count() - 1){
            sequence_.append(Segment());
            currentIndex_++;
        }else{
            sequence_.insert(currentIndex_ + 1,Segment());
        }
        return true;
    }
    return false;
}

void ProfileSequence::updateSegment(SEGMENT::MODE segmentMode, uint16_t grillTemperature, uint16_t productTemperature, uint16_t timeSeconds)
{
    Segment segment(segmentMode, grillTemperature, productTemperature, timeSeconds);
    sequence_.replace(currentIndex_, segment);
}

void ProfileSequence::updateSegment(const ProfileSequence::Segment &segment)
{
    sequence_[currentIndex_] = segment;
}

bool ProfileSequence::removeSegment()
{
    if(!sequence_.isEmpty()){
        if(currentIndex_ == 0){
            sequence_.removeFirst();
        }else if( currentIndex_ == sequence_.count() - 1){
            sequence_.removeLast();
            currentIndex_--;
        }else{
            sequence_.remove(currentIndex_);
        }
        return true;
    }
    return false;
}

void ProfileSequence::setName(const QString &name)
{
    name_ = name;
}

QString ProfileSequence::name() const
{
    return name_;
}

void ProfileSequence::unpack(uint8_t *buffer)
{

}

void ProfileSequence::pack(uint8_t *buffer)
{

}

 const ProfileSequence::Segment ProfileSequence::currentSegment() const
{
    if(isEmpty()){
        return ProfileSequence::Segment(); // Will return as STOP default.
    }else{
        return sequence_.at(currentIndex_);
    }
 }

 const ProfileSequence::Segment ProfileSequence::peekNextSegment() const
 {
    if((currentIndex_ + 1) < (sequence_.count())){
        return sequence_.at(currentIndex_ + 1);
    }else{
        return ProfileSequence::Segment(); // Will return as STOP default.
    }
 }

 const ProfileSequence::Segment ProfileSequence::peekLastSegment() const
 {
     if((currentIndex_ - 1) >= 0){
         return sequence_.at(currentIndex_ - 1);
     }else{
         return ProfileSequence::Segment(); // Will return as STOP default.
     }
 }
