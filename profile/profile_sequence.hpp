#pragma once
#include <QVector>
#include "enums.hpp"
#include <QString>




// Contains a vector sequence of segment commands

class ProfileSequence
{
public:
    ProfileSequence();



    // This is an action segment
    struct Segment {
        Segment():
            mode(SEGMENT::MODE::STOP),
            grillTemp(0),
            productTemp(0),
            time(0)
        {}

        Segment(SEGMENT::MODE segmentMode, uint16_t grillTemperature, uint16_t productTemperature, uint16_t timeSeconds):
            mode(segmentMode),
            grillTemp(grillTemperature),
            productTemp(productTemperature),
            time(timeSeconds)
        {}

        SEGMENT::MODE mode;
        uint16_t grillTemp;
        uint16_t productTemp;
        uint16_t time;
    };

    // Movement - returns true if moved. False if at limit
    bool nextSegment();
    bool previousSegment();
    void firstSegment();
    bool moveToSegment(int index);

    bool isEmpty() const;

    int count() const {return sequence_.count();}

    // commands
    // returns true if succeeds
    bool addNewSegment();
    void updateSegment(SEGMENT::MODE segmentMode, uint16_t grillTemperature, uint16_t productTemperature, uint16_t timeSeconds);
    void updateSegment(const Segment& segment);
    bool removeSegment();

    void setName(const QString& name);
    QString name() const;

    static constexpr int MAX_NAME_LENGTH = 32;

    void unpack(uint8_t* buffer);
    void pack(uint8_t* buffer);

    // Actions
     const Segment currentSegment() const;

     const Segment peekNextSegment() const;

     const Segment peekLastSegment() const;


private:
    QString name_;
    // Contains a series of action segments
    QVector<Segment>  sequence_;

    int currentIndex_;

    static constexpr uint8_t MAX_SEGMENT = 16;
};
