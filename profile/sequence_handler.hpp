#pragma once
#include "profile_sequence.hpp"
#include "settings.hpp"
#include "status.hpp"
#include <QTimer>
#include <QObject>
#include <QList>
#include <QString>
#include "controller.hpp"



class SequenceHandler : public QObject
{
    Q_OBJECT
public:
    SequenceHandler(Settings& settings, const Status& status);


    bool loadSequence(ProfileSequence sequence);


    void clearAnnunciation();
    bool isAnnunciated() const;

    void run();
    void pause();
    void stop();

    QString segmentTimeRemainingStr() const;
    QString transitionTimeStr() const;
    QString nextSegmentStr() const;
    QString currentSegmentStr() const;
    QString sequenceNameStr() const;

    static const QStringList SEGMENT_STR;


signals:
    void annunciationSignal();


private slots:
    void serviceSlot();
    void transitionTimerSlot();
    void segmentTimerSlot();


private:
    Settings& settings_;
    const Status& status_;
    ProfileSequence sequence_;
    QTimer serviceTimer_;
    QTimer transitionTimer_;
    QTimer segmentTimer_;

    bool annunciationFlag_;
    bool clearAnnunciationAck_;

    Controller firingControl_;
    Controller grillControl_;

    bool run_;

    static constexpr uint32_t SERVICE_TIME_MS = 1000;
    static constexpr uint32_t SECOND_TIME_MS = 1000;
    static constexpr uint32_t TRANSITION_TIME_MS = 5000;
    static constexpr uint32_t SEGMENT_TIMEOUT_MS = 10 * 60 * 1000;

    // Timer Handling
    void startNextStateTransition(uint32_t transitionTime);
    void startSegmentTimer(uint32_t segmentTime);


    // Helpers
    bool runPermissive() const;
    bool flameIsOn() const;


    // Only handles calls for heat. Flame on and off is handled in an external class
    void allStop();

    void burnerOff();

    void resetModulation();


    // Segment Control Definitions. If returns true move to next segment.

    // Stop fire
    bool stopSegment();

    // Heat Grill to grill temp SP
    bool heatGrillSegment();

    // Heat product to Product Temp SP at the Grill Temp SP
    bool cookProductSegment();

    // Heat product at the grill temp SP for the specified time
    bool keepProductSegment();

    // Heat product at grill setpoint indefinately
    bool keepWarmSegment();

    // Stop Fire, for the specified time.
    bool restProductSegment();

    // Audible and visual annunciation of completion or time to flip.
    bool announceSegment();

    bool manualPositionSegment();

};


