#pragma once

#include <QObject>
#include <QVector>
#include "profile/profile_sequence.hpp"

// Load recipes to and from memory

class SementLoader : public QObject
{
    Q_OBJECT
public:
    explicit SementLoader(QObject *parent = nullptr);

    void load();
    void store();

signals:

private:
    QVector<ProfileSequence> sequenceList_;

    static constexpr int MAX_SEQUENCES = 20;

};


