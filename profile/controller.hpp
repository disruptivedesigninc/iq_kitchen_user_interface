#pragma once
#include <stdint.h>

class Controller
{
public:
    Controller(bool directActing);

    // outputs control calculation based on process variable and setpoint
    uint8_t modulate(const float& process, const float& processSP);

    void reset();

    // Call once per second to update internal time calculations
    void updateTimeSec();


    void setProportional(float proportional){ proportionalBand_ = proportional;}
    void setIntegral(float integral) { integralGain_ = integral;}
    void setResetRange(float range) { integralResetRange_ = range;}


private:
    float proportionalBand_;
    float integralGain_;
    float integralResetRange_;
    bool directActing_;
    uint32_t currentTimeSec_;
    uint32_t lastTimeSec_;
    float accumulatedIntegral_;
    float lastIntegralSample_;
    float lastRawOutput_;



    static constexpr uint8_t HIGH_OUTPUT_LIMIT = 100;
    static constexpr uint8_t LOW_OUTPUT_LIMIT = 0;

    uint8_t gate(float value);

    bool integralJacketingActive(float error);


};

