#include "sequence_handler.hpp"
#include "profile_sequence.hpp"




const QStringList SequenceHandler::SEGMENT_STR = {"Stop","Heating Grill",
                                                     "Cooking Product",
                                                     "Holding Temperature",
                                                     "Warming",
                                                     "Resting",
                                                     "Segment Complete",
                                                     "Manual Position"};



SequenceHandler::SequenceHandler(Settings& settings, const Status& status):
    settings_(settings),
    status_(status),
    sequence_(),
    serviceTimer_(),
    transitionTimer_(),
    segmentTimer_(),
    annunciationFlag_(false),
    clearAnnunciationAck_(false),
    firingControl_(true),
    grillControl_(false),
    run_(false)
{
    connect(&serviceTimer_, &QTimer::timeout, this, &SequenceHandler::serviceSlot);
    serviceTimer_.start(SERVICE_TIME_MS);

    connect(&segmentTimer_, &QTimer::timeout, this, &SequenceHandler::segmentTimerSlot);
    segmentTimer_.setSingleShot(true);
    //segmentTimer_.start(SECOND_TIME_MS);

    connect(&transitionTimer_, &QTimer::timeout, this, &SequenceHandler::transitionTimerSlot);
    transitionTimer_.setSingleShot(true);
    //transitionTimer_.start(TRANSITION_TIME_MS);
}

bool SequenceHandler::loadSequence(ProfileSequence sequence)
{
    sequence_ = sequence;
    sequence_.firstSegment();

    return !sequence_.isEmpty();
}

void SequenceHandler::clearAnnunciation()
{
    clearAnnunciationAck_ = true;
}

bool SequenceHandler::isAnnunciated() const
{
    return annunciationFlag_;
}

void SequenceHandler::run()
{
    sequence_.firstSegment();
    run_ = true;
    // Set to beggining
}

void SequenceHandler::pause()
{
    // hold timers and prevent sequence change.???
}

void SequenceHandler::stop()
{
    allStop();
}

QString SequenceHandler::segmentTimeRemainingStr() const
{
    if(segmentTimer_.isActive()){
        return QString::number(static_cast<int>((segmentTimer_.remainingTime() / 1000)));
    }else{
        return "0";
    }
}

QString SequenceHandler::transitionTimeStr() const
{
    if(transitionTimer_.isActive()){
        return QString::number(static_cast<int>((transitionTimer_.remainingTime() / 1000)));
    }else{
        return "0";
    }
}

QString SequenceHandler::nextSegmentStr() const
{

   return SEGMENT_STR.at((int)sequence_.peekNextSegment().mode);
}

QString SequenceHandler::currentSegmentStr() const
{

    return SEGMENT_STR.at((int)sequence_.currentSegment().mode);
}

QString SequenceHandler::sequenceNameStr() const
{
    return sequence_.name();
}

void SequenceHandler::serviceSlot()
{

    // Service time
    firingControl_.updateTimeSec();
    grillControl_.updateTimeSec();

    if(!run_){
        return;
    }

    // check transition status
    bool transitionCall = false;

    switch(sequence_.currentSegment().mode){
        case SEGMENT::MODE::STOP:{
        transitionCall = stopSegment();
        break;
        }
        case SEGMENT::MODE::HEAT_GRILL:{
        transitionCall = heatGrillSegment();
            break;
        }
        case SEGMENT::MODE::COOK_PRODUCT:{
        transitionCall = cookProductSegment();
            break;
        }
        case SEGMENT::MODE::KEEP_PRODUCT:{
        transitionCall = keepProductSegment();
            break;
        }
        case SEGMENT::MODE::KEEP_WARM:{
        transitionCall = keepWarmSegment();
            break;
        }
        case SEGMENT::MODE::REST_PRODUCT:{
        transitionCall = restProductSegment();
            break;
        }
        case SEGMENT::MODE::ANNOUNCE:{
        transitionCall = announceSegment();
            break;
        }
        case SEGMENT::MODE::MANUAL:{
        transitionCall = manualPositionSegment();
            break;
        }
    }

    if(transitionCall){
        startNextStateTransition(TRANSITION_TIME_MS);
    }



  }

void SequenceHandler::transitionTimerSlot()
{
    // After the transition timer elapses it simply calls the next sequence
    bool transitionSuccess = sequence_.nextSegment();

    if(!transitionSuccess){
       // The existing state has ended and there are no more. Stop the system.
        allStop();
    }
}

void SequenceHandler::segmentTimerSlot()
{


}

void SequenceHandler::startNextStateTransition(uint32_t transitionTime)
{
    transitionTimer_.start(transitionTime);
}

void SequenceHandler::startSegmentTimer(uint32_t segmentTime)
{
    segmentTimer_.start(segmentTime);
}

bool SequenceHandler::runPermissive() const
{
    return flameIsOn() && (settings_.flameOn == FLAME::COMMAND::RUN);
}

bool SequenceHandler::flameIsOn() const
{
    return status_.flameStatus == FLAME::STATUS::FLAME_ON;
}

void SequenceHandler::allStop()
{
    burnerOff();
    settings_.grillPosition = 0;
    transitionTimer_.stop();
    segmentTimer_.stop();
    run_ = false;
}

void SequenceHandler::burnerOff()
{
    settings_.callForHeat = FLAME::HEAT::NONE;
    settings_.firingRate = 0;
    resetModulation();
}

void SequenceHandler::resetModulation()
{
    grillControl_.reset();
    firingControl_.reset();
}

bool SequenceHandler::stopSegment()
{
    // Once stopped we can't start again until sequence is reset.
    allStop();
    return false;
}

bool SequenceHandler::heatGrillSegment()
{
    if(!runPermissive()){
        allStop();
        return false;
    }
    // Grill temp
    settings_.grillPosition = grillControl_.modulate(status_.temperatureArr.at(TEMP::APPLICATION::GRILL), sequence_.currentSegment().grillTemp);
    settings_.firingRate = firingControl_.modulate(status_.temperatureArr.at(TEMP::APPLICATION::GRILL), sequence_.currentSegment().grillTemp);

    bool completeCondition = (status_.temperatureArr.at(TEMP::APPLICATION::GRILL) >= sequence_.currentSegment().grillTemp);

    return completeCondition;
}

bool SequenceHandler::cookProductSegment()
{
    if(!runPermissive()){
        allStop();
        return false;
    }
    // Grill temp
    settings_.grillPosition = grillControl_.modulate(status_.temperatureArr.at(TEMP::APPLICATION::GRILL), sequence_.currentSegment().grillTemp);
    settings_.firingRate = firingControl_.modulate(status_.temperatureArr.at(TEMP::APPLICATION::GRILL), sequence_.currentSegment().grillTemp);

    bool completeCondition = (status_.temperatureArr.at(TEMP::APPLICATION::PRODUCT) >= sequence_.currentSegment().productTemp);

    return completeCondition;

}

bool SequenceHandler::keepProductSegment()
{
    if(!runPermissive()){
        allStop();
        return false;
    }
    if(!segmentTimer_.isActive()){
        segmentTimer_.start(sequence_.currentSegment().time * 1000);
    }
    // Grill temp
    settings_.grillPosition = grillControl_.modulate(status_.temperatureArr.at(TEMP::APPLICATION::GRILL), sequence_.currentSegment().grillTemp);
    settings_.firingRate = firingControl_.modulate(status_.temperatureArr.at(TEMP::APPLICATION::GRILL), sequence_.currentSegment().grillTemp);

    bool completeCondition = (segmentTimer_.remainingTime() <= 0);
    if(completeCondition){
        segmentTimer_.stop();
    }

    return completeCondition;
}

bool SequenceHandler::keepWarmSegment()
{
    if(!runPermissive()){
        allStop();
        return false;
    }

    if(!segmentTimer_.isActive()){
        segmentTimer_.start(sequence_.currentSegment().time * 1000);
    }

    settings_.grillPosition = grillControl_.modulate(status_.temperatureArr.at(TEMP::APPLICATION::GRILL), sequence_.currentSegment().grillTemp);
    settings_.firingRate = firingControl_.modulate(status_.temperatureArr.at(TEMP::APPLICATION::GRILL), sequence_.currentSegment().grillTemp);

    // Always stay here until system paused or stopped or new sequence ran or timeout.

    bool completeCondition = (segmentTimer_.remainingTime() <= 0);


    // Stop Fire
    if(completeCondition){
        segmentTimer_.stop();
        allStop();
    }

    return completeCondition;
}

bool SequenceHandler::restProductSegment()
{
    // Stop Fire
    burnerOff();


    // Increase grill height
    settings_.grillPosition = 100;

    if(!segmentTimer_.isActive()){
        segmentTimer_.start(sequence_.currentSegment().time * 1000);
    }


    bool completeCondition = (segmentTimer_.remainingTime() <= 0);
    if(completeCondition){
        segmentTimer_.stop();
    }

    return completeCondition;
}

bool SequenceHandler::announceSegment()
{


    // Will hold at minimum flame output
    settings_.firingRate = 0;

    // Maximum height
    settings_.grillPosition = 100;

    if(!segmentTimer_.isActive()){
        segmentTimer_.start(SEGMENT_TIMEOUT_MS);
    }


    if(segmentTimer_.remainingTime() <= 0){
        segmentTimer_.stop();
        allStop();
        return true;
    }else if(clearAnnunciationAck_){
        clearAnnunciationAck_ = false;
        annunciationFlag_ = false;
        return true;
    }else{

    // Flag annunciation
    annunciationFlag_ = true;
    clearAnnunciationAck_ = false;
    return false;
    }
}

bool SequenceHandler::manualPositionSegment()
{
    if(!runPermissive()){
        allStop();
        return false;
    }

    //Overloaded for 0 to 100% manual positon.
    settings_.grillPosition = sequence_.currentSegment().grillTemp;
    settings_.firingRate = sequence_.currentSegment().productTemp;

    resetModulation();

    if(sequence_.currentSegment().time != 0){
    if(!segmentTimer_.isActive()){
        segmentTimer_.start(sequence_.currentSegment().time * 1000);
    }
    bool completeCondition = (segmentTimer_.remainingTime() <= 0);


    // Stop Fire
    if(completeCondition){
        segmentTimer_.stop();
        allStop();
    }

    return completeCondition;
    }
    return false;
}
