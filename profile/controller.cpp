#include "controller.hpp"
#include <cstdlib>

Controller::Controller(bool directActing):
     proportionalBand_(0.0f),
     integralGain_(0.0f),
     integralResetRange_(0.0f),
     directActing_(directActing),
     currentTimeSec_(0),
     accumulatedIntegral_(0.0f),
     lastIntegralSample_(0.0f),
     lastRawOutput_(LOW_OUTPUT_LIMIT)
{
    reset();
}

uint8_t Controller::modulate(const float &process, const float &processSP)
{

    // Time
    float deltaTime = currentTimeSec_ - lastTimeSec_;

    // Time interval too small
    if(deltaTime < 0.1f){
        return gate(lastRawOutput_);
    }

    lastTimeSec_ = currentTimeSec_;

    // Error
    float error = 0.0f;
    if(directActing_){
        error = process - processSP;
    }else{
        error = processSP - process;
    }



    //Integral term
    float integralTerm = 0.0f;
    if(std::abs(error)>= integralResetRange_){
        // integral reset range
        accumulatedIntegral_ = 0.0f;
        lastIntegralSample_ = error;
        integralTerm = 0.0f;
    }else if(integralJacketingActive(error)){
        integralTerm = (1/integralGain_) * accumulatedIntegral_;
    } else if(integralGain_ < 0.1f){
        integralTerm = 0.0f;
    }else{
        // Calculate integral
        accumulatedIntegral_ += ((deltaTime/60.0f) * (lastIntegralSample_ + error) / 2.0f);
        integralTerm = (1/integralGain_) * accumulatedIntegral_;
    }


    float proportionalGain = (proportionalBand_ < 0.1f)? (1000.0f): (100.0f/proportionalBand_);


    lastRawOutput_ = (proportionalGain * error) + (proportionalGain * integralTerm);


    return gate(lastRawOutput_);
}

void Controller::reset()
{
    currentTimeSec_ = 0;
    lastTimeSec_ = 0;
    lastRawOutput_ = LOW_OUTPUT_LIMIT;
    accumulatedIntegral_ = 0.0f;
}

void Controller::updateTimeSec()
{
    currentTimeSec_++;
}

uint8_t Controller::gate(float value)
{
    uint8_t output = 0;
    if(value > HIGH_OUTPUT_LIMIT){
        output = HIGH_OUTPUT_LIMIT;
    }else if (value < LOW_OUTPUT_LIMIT){
        output = LOW_OUTPUT_LIMIT;
    }else{
        output = static_cast<uint8_t>(value);
    }

    return output;
}

bool Controller::integralJacketingActive(float error)
{
    bool maxOutputAchieved = (lastRawOutput_ >= HIGH_OUTPUT_LIMIT);
    bool minOutputAchieved = (lastRawOutput_ <= LOW_OUTPUT_LIMIT);

    bool aboveSP = false;
    bool belowSP = false;
    bool atSP = (error == 0.0f);

    if(directActing_){
        aboveSP = (error < 0.0f);
        belowSP = (error > 0.0f);
    }else{
        aboveSP = (error > 0.0f);
        belowSP = (error < 0.0f);
    }

    return ((maxOutputAchieved && !aboveSP) || (minOutputAchieved && !belowSP)) && !atSP;
}
