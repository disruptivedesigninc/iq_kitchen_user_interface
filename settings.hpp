#pragma once
#include <stdint.h>
#include "enums.hpp"
#include <algorithm>
#include <cstring>

struct Settings {
    Settings(){
        reset();
    };

    // Fan control
    uint8_t fanOnSetpoint;
    uint8_t fanDeadband;

    // Control
    uint8_t firingRate;
    uint8_t grillPosition;
    uint8_t flameOn;
    uint8_t callForHeat;


    void reset() {
         fanOnSetpoint = 0;
         fanDeadband = 0;
         firingRate = 0;
         grillPosition = 0;
         flameOn = 0;
         callForHeat = 0;
    }

    static constexpr uint16_t size() {
        return  sizeof( fanOnSetpoint) +
                sizeof( fanDeadband) +
                sizeof(firingRate) +
                sizeof(grillPosition) +
                sizeof(flameOn) +
                sizeof(callForHeat);

    }

    void pack(uint8_t* buffer){

        std::memcpy(buffer , (uint8_t*)&fanOnSetpoint, sizeof(fanOnSetpoint));
        buffer += sizeof(fanOnSetpoint);

        std::memcpy(buffer , (uint8_t*)&fanDeadband ,sizeof(fanDeadband));
        buffer += sizeof(fanDeadband);

        std::memcpy(buffer, (uint8_t*)&firingRate,sizeof(firingRate)) ;
        buffer += sizeof(firingRate);

        std::memcpy(buffer, (uint8_t*)&grillPosition,sizeof(grillPosition) );
        buffer += sizeof(grillPosition);

        std::memcpy(buffer, (uint8_t*)&flameOn,sizeof(flameOn) );
        buffer += sizeof(flameOn);

        std::memcpy(buffer, (uint8_t*)&callForHeat, sizeof(callForHeat));
        buffer += sizeof(callForHeat);
    }

    void unpack(uint8_t* buffer){
        std::memcpy((uint8_t*)&fanOnSetpoint, buffer,sizeof(fanOnSetpoint));
        buffer += sizeof(fanOnSetpoint);

        std::memcpy((uint8_t*)&fanDeadband, buffer ,sizeof(fanDeadband) );
        buffer += sizeof(fanDeadband);


        std::memcpy((uint8_t*)&firingRate, buffer ,sizeof(firingRate) );
        buffer += sizeof(firingRate);

        std::memcpy((uint8_t*)&grillPosition, buffer ,sizeof(grillPosition) );
        buffer += sizeof(grillPosition);

        std::memcpy((uint8_t*)&flameOn, buffer ,sizeof(flameOn) );
        buffer += sizeof(flameOn);

        std::memcpy((uint8_t*)&callForHeat, buffer, sizeof(callForHeat));
        buffer += sizeof(callForHeat);

    }

};
