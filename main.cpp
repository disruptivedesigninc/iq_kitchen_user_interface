#include "mainwindow.h"

#include <QApplication>
#include <QFontDatabase>
#include <QPixmap>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);




    QFontDatabase fontDB;
    fontDB.addApplicationFont(":/fonts/fonts/Roboto.ttf");
    fontDB.addApplicationFont(":/fonts/fonts/RobotoBold.ttf");
    fontDB.addApplicationFont(":/fonts/fonts/RobotoLight.ttf");
    fontDB.addApplicationFont(":/fonts/fonts/RobotoMedium.ttf");


    MainWindow w;
    w.setFixedSize(LCD_WIDTH, LCD_HEIGHT);
    w.show();
    w.raise();
    return a.exec();
}
