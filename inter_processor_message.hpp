#pragma once
#include <QObject>
#include <QTimer>
#include "settings.hpp"
#include "status.hpp"
#include <QSerialPort>
#include <QDataStream>

class InterProcessorMessage: public QObject
{
    Q_OBJECT
public:
    InterProcessorMessage(Settings& settings, Status& status);

public slots:
    void read();
    void write();


private:
    QSerialPort port_;
    Settings& settings_;
    Status& status_;
    QTimer writeCycleTimer_;
    uint32_t errorCount_;
    uint32_t readSuccessCount_;

    static constexpr uint16_t settingPacketSize() {
        //return COMMS::PREAMBLE_SIZE + Settings::size() + COMMS::CRC_SIZE;
        return Settings::size();
    }
    static constexpr uint16_t statusPacketSize() {
        //return COMMS::PREAMBLE_SIZE + Status::size() + COMMS::CRC_SIZE;
        return Status::size();
    }

    static constexpr uint16_t settingCRCIndex() {
        return (settingPacketSize() - COMMS::CRC_SIZE) - 1;
    }

    static constexpr uint16_t statusCRCIndex() {
        return (statusPacketSize() - COMMS::CRC_SIZE) - 1;
    }

    // Will callback every 10 milliseconds to check it buffer has been fully received before reading it in.
    static const uint32_t READ_CHECK_MS = 1100;
    // Retries for writting out if buffer is busy.
    static const uint32_t WRITE_CHECK_MS = 230;

    // How often settings get pushed to the M4 side.
    static const uint32_t WRITE_CYCLE_MS = 1000;

    // A7 is the master of this bus.
    // When the A7 sends the settings over to the M4, this will trigger a response for the M4 to send
    // its status back. The M4 won't send data unless it first receives a packet from the M7.

};

