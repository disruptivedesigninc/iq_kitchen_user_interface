#pragma once
#include <QColor>



// Color
#define ACCENT_BLUE #4BC3CF


// Layouts
#define LCD_WIDTH 800
#define LCD_HEIGHT 480

#define NAVIGATION_BAR_WIDTH 60
#define NAVIGATION_BAR_HEIGHT LCD_HEIGHT

#define PAGE_WIDTH (LCD_WIDTH - NAVIGATION_BAR_WIDTH)
#define PAGE_HEIGHT LCD_HEIGHT


// Timing
#define SPLASH_TIME_MS 1000
