QT       += core gui \
        serialport \
        multimedia


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    inter_processor_message.cpp \
    main.cpp \
    mainwindow.cpp \
    navigationbar.cpp \
    pages/auto_mode_page.cpp \
    pages/manualmode.cpp \
    pages/profile_page.cpp \
    pages/readinputevents.cpp \
    pages/setting_page.cpp \
    pagewidget.cpp \
    profile/controller.cpp \
    profile/profile_sequence.cpp \
    profile/sement_loader.cpp \
    profile/sequence_handler.cpp \
    widgets/builder_list.cpp \
    widgets/button_box.cpp \
    widgets/combobox.cpp \
    widgets/control_box.cpp \
    widgets/list_view.cpp \
    widgets/name_row.cpp \
    widgets/segment_dialog.cpp \
    widgets/setting_combobox.cpp \
    widgets/setting_spinbox.cpp \
    widgets/setting_timebox.cpp \
    widgets/spinbox.cpp

HEADERS += \
    defines.hpp \
    enums.hpp \
    inter_processor_message.hpp \
    mainwindow.h \
    navigationbar.hpp \
    pages/auto_mode_page.hpp \
    pages/manualmode.hpp \
    pages/profile_page.hpp \
    pages/readinputevents.hpp \
    pages/setting_page.hpp \
    pagewidget.hpp \
    profile/controller.hpp \
    profile/profile_sequence.hpp \
    profile/sement_loader.hpp \
    profile/sequence_handler.hpp \
    settings.hpp \
    status.hpp \
    widgets/Util.hpp \
    widgets/builder_list.hpp \
    widgets/button_box.hpp \
    widgets/combobox.hpp \
    widgets/control_box.hpp \
    widgets/list_view.hpp \
    widgets/name_row.hpp \
    widgets/segment_dialog.hpp \
    widgets/setting_combobox.hpp \
    widgets/setting_spinbox.hpp \
    widgets/setting_timebox.hpp \
    widgets/spinbox.hpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc

DISTFILES +=
