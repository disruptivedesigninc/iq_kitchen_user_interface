#include "mainwindow.h"
#include <QTimer>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      settings_(),
      status_(),
      comms_(settings_, status_),
      window_(settings_, status_, this),
      inputEvents_(),
      rotaryClick_(this)
{  
    // The window of the LCD screen size.
    window_.setFixedSize(LCD_WIDTH, LCD_HEIGHT);

    // Set system settings
    settings_.fanOnSetpoint = 40;
    settings_.fanDeadband = 5;


    // Sound
    rotaryClick_.setSource(QUrl::fromLocalFile(":/audio/audio/click1.wav"));
    rotaryClick_.setLoopCount(1);
    rotaryClick_.setVolume(1);



    connect(&inputEvents_, SIGNAL(rotary1Up()), &window_, SLOT(increaseGrill()));
    connect(&inputEvents_, SIGNAL(rotary1Down()), &window_, SLOT(decreaseGrill()));
    connect(&inputEvents_, SIGNAL(rotary2Up()), &window_, SLOT(increaseFiringRate()));
    connect(&inputEvents_, SIGNAL(rotary2Down()), &window_, SLOT(decreaseFiringRate()));
    connect(&inputEvents_, SIGNAL(button1()), &window_, SLOT(button1Press()));
    connect(&inputEvents_, SIGNAL(button2()), &window_, SLOT(button2Press()));
    connect(&inputEvents_, SIGNAL(rotarySoundTrigger()), this, SLOT(rotarySoundSlot()));
    connect(&inputEvents_, SIGNAL(buttonSoundTrigger()), this, SLOT(buttonSoundSlot()));
}

MainWindow::~MainWindow()
{
}

void MainWindow::rotarySoundSlot()
{
    if(!rotaryClick_.isPlaying()){
        rotaryClick_.play();
    }
}

void MainWindow::buttonSoundSlot()
{
    if(!rotaryClick_.isPlaying()){
        rotaryClick_.play();
    }
}




bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::KeyPress){
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        switch(keyEvent->key()){
        case Qt::Key::Key_Up:

            break;
        case Qt::Key::Key_Down:

            break;

        case Qt::Key::Key_Return:

            break;
        default:
            break;
        }

        return true;
    }else{
        return QObject::eventFilter(obj, event);
    }
}

